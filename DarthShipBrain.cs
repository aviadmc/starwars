﻿using UnityEngine;
using StarWars.Actions;
using Infra.Utils;

namespace StarWars.Brains {
    public class DarthShipBrain : SpaceshipBrain {
        private int timer = 0;
        public override string DefaultName {
            get {
                return "DarthShip";
            }
        }
        public override Color PrimaryColor {
            get {
                return new Color((float)0xAA / 0xFF, (float)0xEE / 0xFF, (float)0xBB / 0xFF, 1f);
            }
        }
        public override SpaceshipBody.Type BodyType {
            get
            {
                return SpaceshipBody.Type.XWing;
            }
        }
        private const int SAFE_DISTANCE = 3;
        private const int TIMER = 20;

        public override Action NextAction() {
            if (isBeenShotAt()) {
                if (spaceship.CanRaiseShield) {
                    timer = TIMER;
                    return ShieldUp.action;
                }
                else if (spaceship.CanShoot) return Shoot.action;
                else return TurnRight.action;

            }
            if (spaceship.IsShieldUp) {
                timer--;
                if(timer <= 0) return ShieldDown.action;
            }
            var nearestShip = findNearsetShip();
            if (nearestShip == null) return DoNothing.action;
            var pos = spaceship.ClosestRelativePosition(nearestShip);
            var forwardVector = spaceship.Forward;
            var angle = pos.GetAngle(forwardVector);
            if (angle >= 10) return TurnLeft.action;
            if (angle <= -10) return TurnRight.action;
            if (spaceship.CanShoot) return Shoot.action;
            else if (!spaceship.IsShieldUp && spaceship.CanRaiseShield && pos.magnitude <= SAFE_DISTANCE) {
                timer = TIMER;
                return ShieldUp.action;
            }
            else return TurnLeft.action;

        }
        private Spaceship findNearsetShip() {
            var ships = Space.Spaceships;
            Spaceship nearestShip = null;

            foreach (var ship in ships) {
                if (ship.IsAlive)
                    if (nearestShip == null || spaceship.ClosestRelativePosition(ship).magnitude < spaceship.ClosestRelativePosition(nearestShip).magnitude) {
                        if (ship == spaceship) continue;
                        nearestShip = ship;
                    }
            }
            return nearestShip;
        }
        private bool isBeenShotAt() {
            var shots = Space.Shots;
            Shot nearestShot = null;
            foreach (var shot in shots) {
                if (shot.IsAlive)
                    if (nearestShot == null || spaceship.ClosestRelativePosition(shot).magnitude < spaceship.ClosestRelativePosition(nearestShot).magnitude) {
                        var isMyShot = shot.Forward - spaceship.Forward;
                        if (!(isMyShot.x <= 1 && isMyShot.y <= 1))
                            nearestShot = shot;
                    }
            }
            if (nearestShot != null)
                return (spaceship.ClosestRelativePosition(nearestShot).magnitude <= SAFE_DISTANCE);
            else return false;
        }
    }
}
