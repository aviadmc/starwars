﻿using UnityEngine;
using StarWars.Actions;
using Infra.Utils;

namespace StarWars.Brains {
public class DefenderBrain : SpaceshipBrain {
    public override string DefaultName {
        get {
            return "Defender";
        }
    }

    public override Color PrimaryColor {
        get {
            return new Color((float)0x38 / 0xFF, (float)0x49 / 0xFF, (float)0x206 / 0xFF, 1f);
        }
    }

    public override SpaceshipBody.Type BodyType {
        get {
            return SpaceshipBody.Type.TieFighter;
        }
    }

    private Spaceship target = null;
    private Spaceship chaser = null;

    /// <summary>
    /// If the defender feels attacked - it turns on shield if it can, else it starts circling right.
    /// The defender selects all the time the closest ship as target, and try to shoot it.
    /// </summary>
    public override Action NextAction() {
        // Find curr target and chaser:
        float distance = 100;
        foreach (var ship in Space.Spaceships) {
            // Make sure not to target self or dead spaceships, and chooses the closest one:
            if (spaceship != ship && ship.IsAlive) {
                if (spaceship.ClosestRelativePosition(ship).magnitude < distance) {
                    target = ship;
                    distance = spaceship.ClosestRelativePosition(ship).magnitude;
                }
                var angle = ship.Forward.GetAngle(ship.ClosestRelativePosition(spaceship));
                if (angle < 10 && angle > -10) { 
                    chaser = ship;
                }
            }
        }

        if (chaser != null && (!spaceship.IsShieldUp) &&
                spaceship.ClosestRelativePosition(chaser).magnitude < 6){
            return spaceship.CanRaiseShield ? ShieldUp.action : TurnRight.action;
        }

        if (target != null) {
            // try to kill it
            var pos = spaceship.ClosestRelativePosition(target);
            var forwardVector = spaceship.Forward;
            var angle = pos.GetAngle(forwardVector);
            if (angle >= 10) return TurnLeft.action;
            if (angle <= -10) return TurnRight.action;
            if (distance < 20 && (!target.IsShieldUp || target.Energy < 3)) { 
                return spaceship.CanShoot ? Shoot.action : DoNothing.action;
            }
        }
        return DoNothing.action;
    }
}
}